//
//  ViewController.swift
//  Meal
//
//  Created by Алина Ангерова on 22/01/2019.
//  Copyright © 2019 Алина Ангерова. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var headerMealView: HeaderMealView!
    @IBOutlet weak var tableView: UITableView!
    
    var currentContent: [String] = []
    var ingredients = ["This is a short text.", "This is another text, and it is a little bit longer.", "Wow, this text is really very very long! I hope it can be read completely! Luckily, we are using automatic row height!", "This is a short text.", "This is another text, and it is a little bit longer.", "Wow, this text is really very very long! I hope it can be read completely! Luckily, we are using automatic row height!", "This is a short text.", "This is another text, and it is a little bit longer.", "This is a short text.", "This is another text, and it is a little bit longer.", "Wow, this text is really very very long! I hope it can be read completely! Luckily, we are using automatic row height!", "This is a short text.", "This is another text, and it is a little bit longer.", "Wow, this text is really very very long! I hope it can be read completely! Luckily, we are using automatic row height!", "This is a short text.", "This is another text, and it is a little bit longer."]
    var methods = ["This is a short text.", "This is another text, and it is a little bit longer.Wow, this text is really very very long! I hope it can be read completely! Luckily, we are using automatic row height!", "Wow, this text is really very very long! I hope it can be read completely! Luckily, we are using automatic row height!", "This is a short text.", "This is another text, and it is a little bit longer.", "Wow, this text is really very very long! I hope it can be read completely! Luckily, we are using automatic row height!"]
    var selectedIngredients: [IndexPath] = []
    var selectedMethods: [IndexPath] = []
    var selectedCells: [IndexPath] = []
    var height: CGFloat = 0
    let screenSize = UIScreen.main.bounds
    var initialTableViewHeight: CGFloat = 0

    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 44
        tableView.contentInset = UIEdgeInsets(top: 20.0, left: 0.0, bottom: 0.0, right: 0.0)
        currentContent = ingredients
        tableView.reloadData()
        initialTableViewHeight = tableView.bounds.height
        headerMealView.imageView.image = UIImage(named: "meal_Image")
        headerMealView.nameLabel.text = "Blueberry pie"
        headerMealView.carbslabel.text = "1 net carbs"
        createNotificationCenter()
        

    }
    
    func createNotificationCenter(){
        NotificationCenter.default.addObserver(self,selector: #selector(changeTableViewData), name: NSNotification.Name(rawValue: segmentedControlNotificationKey), object: nil)
    }
    
    @objc func changeTableViewData(){
        if headerMealView.segmentedControl.selectedSegmentIndex == 0{
            currentContent = ingredients
            selectedCells = selectedIngredients
            footerView.isHidden = false
            footerView.frame.size.height = 75
       
        }else{
            currentContent = methods
            selectedCells = selectedMethods
            footerView.isHidden = true
            footerView.frame.size.height = 0
        }
        print("dnfbinrjnejrnb")
        tableView.reloadData()
    }
}

extension ViewController: UITableViewDelegate,UITableViewDataSource{

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentContent.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell
        cell.configure(content: currentContent[indexPath.row])
        if selectedCells.contains(indexPath){
            cell.checkImage.image = UIImage(named: "check")
        }else{
            cell.checkImage.image = UIImage(named: "emptyCheck")
        }
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! TableViewCell

        if selectedCells.contains(indexPath){
            cell.checkImage.image = UIImage(named: "emptyCheck")
            selectedCells.remove(at: selectedIngredients.index(of: indexPath)!)
        }else{
            cell.checkImage.image = UIImage(named: "check")
            selectedCells.append(indexPath)
        }
        
        if currentContent == ingredients{
            selectedIngredients = selectedCells
        }else{
            selectedMethods = selectedCells
        }
        
    }

}

extension UILabel {
    var numberOfVisibleLines: Int {
        let textSize = CGSize(width: CGFloat(self.frame.size.width), height: CGFloat(MAXFLOAT))
        let rHeight: Int = lroundf(Float(self.sizeThatFits(textSize).height))
        let charSize: Int = lroundf(Float(self.font.pointSize))
        return rHeight / charSize
    }
}
