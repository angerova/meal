

import UIKit

class HeaderMealView: UIView {
 
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var carbslabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet var contentView: UIView!
    //initWithFrame to init view from code
    @IBOutlet weak var segmentMethodView: UIView!
    @IBOutlet weak var segmentIngredientsView: UIView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBAction func segmentedControlValueChange(_ sender: Any){
        switch segmentedControl.selectedSegmentIndex
        {
        case 0:
            segmentIngredientsView.backgroundColor = .red
            segmentMethodView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)

            
        case 1:
            segmentIngredientsView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            segmentMethodView.backgroundColor = .red

        default:
            break
        }
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: segmentedControlNotificationKey), object: self)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed("HeaderMealView", owner: self, options: nil)
        addSubview(contentView)
        contentView.backgroundColor = .clear
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        createSegmentedControl()
        
    }
    
    func createSegmentedControl(){
        let font = UIFont.systemFont(ofSize: 16)
        segmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font: font], for: UIControl.State.selected)
        segmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: font], for: UIControl.State.normal)
        
        segmentedControl.backgroundColor = .white
        segmentIngredientsView.backgroundColor = .red
        segmentMethodView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    }
}
