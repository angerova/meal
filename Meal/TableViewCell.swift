

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var checkImage: UIImageView!
    @IBOutlet weak var contentLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configure(content: String){
        checkImage.image = UIImage(named: "emptyCheck")
        contentLabel.text = content
        checkImage.tintColor = .red
    }

}


